const request = require('supertest');
const mongoose = require('mongoose');
const {Guest} = require('../../../models/guests');
const {User} = require("../../../models/users");

let server;
let token;
describe('/api/Guests', () => {
    beforeEach(() => { server = require('../../../index'); });
    afterEach(async () => { 
      server.close(); 
      await Guest.remove({});
});
    describe('GET/', () => {
        it('should return all guests', async () => {
            token = new User().generateAuthToken();   
            const guests = [
              {
                name: 'guest1',
                woreda : 1,
                city: 'city1',
                subcity: 'subcity',
                house_no: 1234,
                phone_no: 'house_no1',
                id_no: 'id_no1',
                purpose: 'purpose1'
              },
              {
                name: 'guest2',
                woreda : 1,
                city: 'city1',
                subcity: 'subcity',
                house_no: 1234,
                phone_no: 'house_no1',
                id_no: 'id_no1',
                purpose: 'purpose1'
              },
              {
                name: 'guest3',
                woreda : 1,
                city: 'city1',
                subcity: 'subcity',
                house_no: 1234,
                phone_no: 'house_no1',
                id_no: 'id_no1',
                purpose: 'purpose1'
              },
            ];
            
            await Guest.collection.insertMany(guests);
      
            const res = await request(server).get('/api/Guests').set("x-auth-token", token);
            
            expect(res.status).toBe(200);
            expect(res.body.length).toBe(3);
            expect(res.body.some(g => g.name === 'guest1')).toBeTruthy();
            expect(res.body.some(g => g.name === 'guest2')).toBeTruthy();
            expect(res.body.some(g => g.name === 'guest3')).toBeTruthy();
          });
    });

    describe('GET /:id', () => {
        let guest;

        beforeEach( async () => {
            guest = new Guest({ 
                name: 'guest1',
                woreda : 1,
                city: 'city1',
                subcity: 'subcity',
                house_no: 1234,
                phone_no: 'house_no1',
                id_no: 'id_no1',
                purpose: 'purpose1' 
            });
            token = new User().generateAuthToken();   
            guest = await guest.save();
          });

         it('should return a guest if valid Id is passed', async () => {
      
            const res = await request(server)
            .get('/api/Guests/'+guest._id)
            .set('x-auth-token', token);
      
            expect(res.status).toBe(200);
            expect(res.body).toHaveProperty('name', guest.name);     
          });

          it('should return a status 400 if the Id is invalid', async () => {
            const res = await request(server)
            .get('/api/Guests/1')
            .set('x-auth-token', token);

            expect(res.status).toBe(400);
          });

          it('should return a status 404 if the Id is invalid', async () => {
            const id = mongoose.Types.ObjectId(); 
            const res = await request(server)
            .get('/api/Guests/'+ id)
            .set('x-auth-token', token);

            expect(res.status).toBe(404);
          });


    });

    describe('POST /', () => {
 
        let guest;
    
        beforeEach(() => {
          token = new User({name : "testing", email:"testing@gmail.com",password : "12345", isManager : false}).generateAuthToken();      
          guest = { 
            name: 'guest1',
            woreda : 1,
            city: 'city1',
            subcity: 'subcity',
            house_no: 1234,
            phone_no: 'house_no1',
            id_no: 'id_no1',
            purpose: 'purpose1' 
        };
        });
    
        const exec = () => { return request(server)
            .post('/api/Guests')
            .set('x-auth-token', token)
            .send( guest );
        };


        it('should return 401 if the user is not logged in', async () => {
          token = ''; 
    
          const res = await exec();
    
          expect(res.status).toBe(401);
        });
    
        it('should return 403 if the user is not a manager', async () => {
              

            const res = await exec();
        
            expect(res.status).toBe(403);
        });

        it('should return 400 if the guest has an invalid input', async () => {
          token = new User({name : "testing", email:"testing@gmail.com",password : "12345", isManager : true}).generateAuthToken();
          guest.woreda = '1234h'; 
          
          const res = await exec();
    
          expect(res.status).toBe(400);
        });
    
    
        it('should save the guest if it is valid', async () => {
          guest = { 
            name: 'guest1',
            woreda : 1,
            city: 'city1',
            subcity: 'subcity',
            house_no: 1234,
            phone_no: 'house_no1',
            id_no: 'id_no1',
            purpose: 'purpose1' 
          };
          token = new User({name : "testing", email:"testing@gmail.com",password : "12345", isManager : true}).generateAuthToken();  
          const res = await request(server)
          .post('/api/Guests')
          .set('x-auth-token', token)
          .send( guest );
          console.log(guest);
    
          guest = await Guest.find({ name: 'guest1' });
    
          expect(guest).not.toBeNull();
          expect(res.status).toBe(200);
        });
    
        it('should return the guest if it is valid', async () => {
          token = new User({name : "testing", email:"testing@gmail.com",password : "12345", isManager : true}).generateAuthToken();
          const res = await exec();
    
          expect(res.body).toHaveProperty('_id');
          expect(res.body).toHaveProperty('name', 'guest1');
        });
    });


});