const request = require('supertest');
const {User} = require('../../../models/users');

let server;
describe('/api/Users', () => {

    beforeEach(() => { server = require('../../../index'); });
    afterEach(() => { 
        server.close(); 
    });

    describe('POST /', () => {
        let token;  
        let user;

        const exec =
         () => {
        return request(server)
            .post('/api/Users')
            .send( user );
        }

        beforeEach(() => {    
        user = { 
            name: 'user12',
            email : 'tested@gmail.com',
            password: '12345678'
        };
        });

        it('should return 400 if the user has an invalid input', async () => {
        user.name = '1234'; 
        
        const res = await exec();

        expect(res.status).toBe(400);
        });


        it('should save the user if it is valid', async () => {
        const res = await exec();

        user = await User.find({ name: 'user12' });

        expect(user).not.toBeNull();
        expect(res.status).toBe(200);
        });

        it('should return the user if it is valid', async () => {
        const res = await exec();

        expect(res.body).toHaveProperty('_id');
        expect(res.body).toHaveProperty('name', 'user12');
        });
    });

});