module.exports = (req, res, next) => {
    if (!req.user.isManager) {
        return res.status(403).send("Forbidden");
    } else {
        next();
    }
}