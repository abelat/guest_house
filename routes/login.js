const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const joi = require('joi');
const {User} = require('../models/users');

router.post('/', async(req, res) => {
    const {error} = validateUser(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    const user = await User.findOne({email : req.body.email});
    if(!user) return res.status(400).send('Invalid Id or Password');

    const valid = bcrypt.compare(req.body.password, user.password);
    if(!valid){
        return res.status(400).send('Invalid Id or Password');
    }
    else{
        console.log("users" + user);
        const token = user.generateAuthToken();
        return res.send(token);
    }
});

function validateUser(user){
    const schema = {
        email: joi.string().email().required(),
        password: joi.string().min(8).required()
    };

    return joi.validate(user, schema);
}

module.exports = router;