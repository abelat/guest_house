const express = require('express');
const router = express.Router();
const {Guest, validate} = require('../models/guests');
const auth = require("../middlewares/auth");
const validateObjectId = require("../middlewares/validateObjectId");
const isManager = require('../middlewares/isManager');


router.get("/", [auth], async (req, res) => {
   const user = await Guest.find().sort("date"); 
   if(!user){
       return res.status(404).send("There is nothing in the database");
   }
   return res.send(user);
});

router.get("/:id", [validateObjectId, auth], async (req, res) => {
   const user = await Guest.findById(req.params.id); 
   if(!user){
       return res.status(404).send("There is no guest with this id");
   }
   return res.send(user);
});

router.post('/', [auth, isManager], async (req, res) => {
    const {error} = validate(req.body);
    if(error){
    return res.status(400).send(error.details[0].message);
    }

    let guest = new Guest({
        name : req.body.name,
        woreda: req.body.woreda,
        city: req.body.city,
        subcity: req.body.subcity,
        house_no: req.body.house_no,
        phone_no: req.body.phone_no,
        id_no: req.body.id_no,
        purpose: req.body.purpose
    });

    guest = await guest.save();

    return res.send(guest);
});

module.exports = router;