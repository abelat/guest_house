const mongoose = require('mongoose');
const jwt = require("jsonwebtoken");
const joi = require('joi');
const _ = require("lodash");

const UserSchema = new mongoose.Schema({
    name : {
        type: String,
        minlength: 5,
        required: true
    },
    email : {
        type : String,
        required: true
    },
    password : {
        type:String,
        required: true,
        minlength:8 
    },
    isManager : {
        type: Boolean,
        required: true,
        default: false
    }
});

UserSchema.methods.generateAuthToken = function () {
    const token = jwt.sign(_.pick(this, ['_id', 'name', 'isManager']), "guestHouseSecurekey");
    return token;
  }

const User = mongoose.model('User', UserSchema);

function validateUser(user){
    const schema = {
        name : joi.string().min(5).required(),
        email: joi.string().email().required(),
        password: joi.string().required()
    };

    return joi.validate(user, schema);
}

module.exports.validate = validateUser;
module.exports.User = User;
