const mongoose = require('mongoose');
const joi = require('joi');

const Guest = mongoose.model("Guest", new mongoose.Schema({
    date: {
        type: Date,
        required: true,
        default: Date.now
    },
    name: {
        type: String,
        minlength: 5,
        required: true
    },
    woreda: {
        type: Number,
        required: true,
        min: 1,
        max: 15
    },
    city: {
        type: String,
        required: true
    },
    subcity: {
        type: String,
        required: true
    },
    house_no: {
        type: Number,
        required: true
    },
    phone_no: {
        type: String,
        required: true
    },
    id_no: {
        type: String,
        required: true
    },
    purpose: {
        type: String,
        required: true
    }
}));

function validateGuest(guest) {
    const schema = {
        name: joi.string(),
        woreda: joi.number(),
        city: joi.string(),
        subcity: joi.string(),
        house_no: joi.number(),
        phone_no: joi.string(),
        id_no: joi.string(),
        purpose: joi.string()

    };
    return joi.validate(guest, schema);
}

module.exports.validate = validateGuest;
module.exports.Guest = Guest;